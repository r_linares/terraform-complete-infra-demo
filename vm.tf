resource "azurerm_virtual_machine" "rlterraformvm" {
    name                  = "rlVM"
    location              = "eastus"
    resource_group_name   = "${azurerm_resource_group.rlterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.rlterraformnic.id}"]
    vm_size               = "Standard_DS1_v2"

    storage_os_disk {
        name              = "rlOsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "rlvm"
        admin_username = "rluser"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/rluser/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCysmL6zKkutOLD/1x5PONQfQNle1nWoV+TbHAPHxhz7fbDiMDun9J+tIOc9AnNck60yX2onnxTxL9DsQ2wUXpXDq2qLdMnWijBr+egLgCkvcMqVpgqNQp2Ii7n3/wbmqmJkhS1LlQ1rPAbB39vDnwr7Gy75n0TWtXDxix9Mimq2WNr9Wao0P0HRLkueMGhqE1FyNCbsITbRvzBBgyUASDU8WIk19B9/sZqsWE5OWkiX67RmzqJUUCZ2XBSEbG+mOWT/IMMJetSTTlwvgN8GcGQZlvmNRJllYXH3C30ElHYxlC47Q/lZ9akp2Ih/tvPuRcGiZmQ1wbhnt+dLNn/iiz3"
        }
    }

    tags {
        environment = "Terraform Demo"
    }
}
