resource "azurerm_resource_group" "rlterraformgroup" {
    name     = "rlResourceGroup"
    location = "eastus"

    tags {
        environment = "Raúl Terraform Demo"
    }
}
