resource "azurerm_subnet" "rlterraformsubnet-01" {
    name                 = "rlSubnet-01"
    resource_group_name  = "${azurerm_resource_group.rlterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.rlterraformnetwork.name}"
    address_prefix       = "10.164.1.0/24"
}

resource "azurerm_subnet" "rlterraformsubnet-02" {
    name                 = "rlSubnet-02"
    resource_group_name  = "${azurerm_resource_group.rlterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.rlterraformnetwork.name}"
    address_prefix       = "10.164.2.0/24"
}

resource "azurerm_subnet" "rlterraformsubnet-03" {
    name                 = "rlSubnet-03"
    resource_group_name  = "${azurerm_resource_group.rlterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.rlterraformnetwork.name}"
    address_prefix       = "10.164.3.0/24"
}
