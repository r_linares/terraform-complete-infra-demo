resource "azurerm_network_interface" "rlterraformnic" {
    name                = "rlNIC"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.rlterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.rlterraformnsg.id}"

    ip_configuration {
        name                          = "rlNicConfiguration"
        subnet_id                     = "${azurerm_subnet.rlterraformsubnet-01.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.rlterraformpublicip.id}"
    }

    tags {
        environment = "Raúl Terraform Demo"
    }
}
