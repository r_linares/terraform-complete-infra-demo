resource "azurerm_virtual_network" "rlterraformnetwork" {
    name                = "rlVnet"
    address_space       = ["10.164.0.0/16"]
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.rlterraformgroup.name}"

    tags {
        environment = "Raúl Terraform Demo"
    }
}
