resource "azurerm_public_ip" "rlterraformpublicip" {
    name                         = "rlPublicIP"
    location                     = "eastus"
    resource_group_name          = "${azurerm_resource_group.rlterraformgroup.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "Raúl Terraform Demo"
    }
}
