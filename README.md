# TUTORIAL DE TERRAFORM PARA CREAR INFRAESTRUCTURA COMO CÓDIGO EN MICROSOFT AZURE

Este repositorio se crea con la finalidad de expresar la idea intuitiva de como utilizar **Hashicorp/Terraform** contra **Microsoft-Azure** y que pueda servir como guía para Start-UP del mismo.

Es de vital importancia entender que este tutorial utiliza el método de conexión "AZ LOGIN" contra **Microsfot Azure**, Si no se desea utilizar este método, puede saltar los pasos 1, 2 y 3 y configurar su conexión contra **Microsoft-Azure** renombrando el fichero "provider.txt" a "provider.tf", éste método es conocido como "Service Principal"

## Pre-requisitos:

* Cliente AZ CLI instalado y configurado en el bastion host que se encargará de hacer los "deployments, asumiendo que el mismo es un Debian Based (Ubuntu 16.04 ó 18.04 LTS)"

1.- Primero modificamos los repositorios
```bash
sudo apt-get install apt-transport-https lsb-release software-properties-common -y
AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    sudo tee /etc/apt/sources.list.d/azure-cli.list
```
2.- Obtenemos la llave de Microsoft
```bash
sudo apt-key --keyring /etc/apt/trusted.gpg.d/Microsoft.gpg adv \
     --keyserver packages.microsoft.com \
     --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF
```
3.- Instalamos AZ CLI
```bash
sudo apt-get update
sudo apt-get install azure-cli
```

* Descargar Binario/Utilitario de TERRAFORM y copiarlo dentro del directorio matriz del usuario del SO

```bash
wget https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip \
     && unzip *.zip \
     && mv terraform /usr/local/bin/. \
     && rm *.zip
```
* Iniciar el utilitario de terraform dentro del folder del proyecto donde se encuentran todos los archivos de infraestructura (.tf) 

```bash
terraform init
```

## Modo de uso

A continuación se presenta el orden de creación y ejecución de cada uno de los ficheros.

**Grupo de Recursos**
En esta primera sección, se crea un grupo de recursos denominado rlResourceGroup en la ubicación eastus:

```bash
resource "azurerm_resource_group" "rlterraformgroup" {
    name     = "rlResourceGroup"
    location = "eastus"

    tags {
        environment = "Terraform Demo"
    }
}
```

**Creación de la red virtual**
Con la siguiente sección se crea una red virtual llamada rlVnet en el espacio de direcciones 10.0.0.0/16:

```bash
resource "azurerm_virtual_network" "rlterraformnetwork" {
    name                = "rlVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.rlterraformgroup.name}"

    tags {
        environment = "Terraform Demo"
    }
}
```
Y la subnet...

```bash
resource "azurerm_subnet" "rlterraformsubnet" {
    name                 = "rlSubnet"
    resource_group_name  = "${azurerm_resource_group.rlterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.rlterraformnetwork.name}"
    address_prefix       = "10.0.2.0/24"
}
```

**Creación de IP Pública**
Para acceder a recursos de Internet, cree una dirección IP pública y asígnela a la VM. Con la siguiente sección se crea una dirección IP pública llamada rlPublicIP:

```bash
resource "azurerm_public_ip" "rlterraformpublicip" {
    name                         = "rlPublicIP"
    location                     = "eastus"
    resource_group_name          = "${azurerm_resource_group.rlterraformgroup.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "Terraform Demo"
    }
}
```

**A continuación creamos el NSG (Network Security Group) el cual es un punto de vital importancia tanto para la infraestructura como tal y el laboratorio en curso**

```bash
resource "azurerm_network_security_group" "rlterraformnsg" {
    name                = "rlNetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.rlterraformgroup.name}"

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags {
        environment = "Terraform Demo"
    }
}
```
**Creación de Tarjeta de Red**

Las tarjetas de interfaz de red (NIC) virtuales conectan la VM a una red virtual determinada, una dirección IP pública y un grupo de seguridad de red. Con la siguiente sección de una plantilla de Terraform se crea un NIC virtual llamado rlNIC que se conecta a los recursos de red virtual que se hayan creado:

```bash
resource "azurerm_network_interface" "rlterraformnic" {
    name                = "rlNIC"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.rlterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.rlterraformnsg.id}"

    ip_configuration {
        name                          = "rlNicConfiguration"
        subnet_id                     = "${azurerm_subnet.rlterraformsubnet.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.rlterraformpublicip.id}"
    }

    tags {
        environment = "Terraform Demo"
    }
}
```

**Creación de la Máquina Virtual**

El último paso es crear una máquina virtual y usar todos los recursos creados. Con la siguiente sección se crea una máquina virtual rlVM y se conecta la NIC virtual rlNIC. Se utiliza la imagen de Ubuntu 16.04-LTS más reciente y se crea un usuario llamado azureuser con la autenticación de contraseña deshabilitada.

```bash
resource "azurerm_virtual_machine" "rlterraformvm" {
    name                  = "rlVM"
    location              = "eastus"
    resource_group_name   = "${azurerm_resource_group.rlterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.rlterraformnic.id}"]
    vm_size               = "Standard_DS1_v2"

    storage_os_disk {
        name              = "rlOsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "rlvm"
        admin_username = "azureuser"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/azureuser/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3Nz{snip}hwhqT9h"
        }
    }

    tags {
        environment = "Terraform Demo"
    }
}
```

Todo este procedimiento puede ser ejecutado uno a uno mediante el comando:

```bash
terraform apply -target [resource]
```
Sin embargo al ejecutar un "terraform apply" de manera global en el directorio donde se encuentran todos los ficheros, terraform es idempotente e inteligente para saber la secuencia explícita en la que se va a ejecutar cada unos de esos archivos .tf


** NOTA: La región utilizada es: east-US "eastus"
